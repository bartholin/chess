import java.util.Scanner;
import java.util.ArrayList;

/**
 * Classe Jeu : classe principale du jeu d'échec.
 * Initialise les joueurs, le plateau, l'ensemble jouable.
 * Appelle l'affichage du plateau et gère l'affichage des pièces jouable.
 * Gère les entrées des joueurs
 *
 * @author Niels BEAUMONT
 * @version v3.0
 */

public class Jeu {

    private Joueur joueurB;
    private Joueur joueurN;
    private Plateau plateau;
    private Piece piece;
    private ArrayList<Piece> ensemblePiece;
    private ArrayList<Piece> ensembleJouable;
    private int xDep;
    private int yDep;
    private int numPiece;
    private Piece pieceJoue;
    private String stringJouable;
    private String stringDeplacement;

    /**
     * Constructeur de la classe jeu, permet d'initialiser les joueurs, le plateau et permet le déroulement du jeu
     */
    public Jeu() {
        this.joueurB = new Joueur('B');
        this.joueurN = new Joueur('N');

        this.plateau = new Plateau();

        this.ensembleJouable = new ArrayList();

        Scanner sc = new Scanner(System.in);

        while ( this.joueurB.getEnsemblePiece().size()>0){

            AfficherPlateau(this.joueurN);

            AfficherJouable(Deplacement.DeplacementDispo(this.plateau, this.joueurB, 'B'));

            ensembleJouable = Deplacement.DeplacementDispo(this.plateau, this.joueurB, 'B');



            do{
                System.out.println("entrez la piece a jouer : ");
                stringJouable = sc.nextLine();
            }while(!verifPiece(stringJouable));

            numPiece = Integer.parseInt(stringJouable)-1;
            System.out.println("entrez le deplacement : ");

            do{
                stringDeplacement = sc.nextLine();
            

                xDep = Deplacement.charIntoInt(stringDeplacement.substring(0,1));
                yDep = Integer.parseInt(stringDeplacement.substring(1))-1;

                System.out.println("numpiece  : " + numPiece + " x : " + xDep + " y : " + yDep);

                if (  numPiece>=0 && numPiece<ensembleJouable.size()){
                    pieceJoue = this.ensembleJouable.get(numPiece);

                    System.out.println("numpiece  : " + numPiece + " x : " + pieceJoue.getX() + " y : " + pieceJoue.getY());

                }
            }while(!Deplacement.DeplacementPiece(pieceJoue,xDep,  yDep, this.joueurB, 'B'));

            plateau.viderCase();


            AfficherPlateau(this.joueurN);


            AfficherJouable(Deplacement.DeplacementDispo(this.plateau, this.joueurN, 'N'));

            ensembleJouable = Deplacement.DeplacementDispo(this.plateau, this.joueurN, 'N');


            do{
                System.out.println("entrez la piece a jouer : ");
                stringJouable = sc.nextLine();
            }while(!verifPiece(stringJouable));

            numPiece = Integer.parseInt(stringJouable)-1;
            System.out.println("entrez le deplacement : ");

            do{
                stringDeplacement = sc.nextLine();
            

                xDep = Deplacement.charIntoInt(stringDeplacement.substring(0,1));
                yDep = Integer.parseInt(stringDeplacement.substring(1))-1;

                System.out.println("numpiece  : " + numPiece + " x : " + xDep + " y : " + yDep);

                if (  numPiece>=0 && numPiece<ensembleJouable.size()){
                    pieceJoue = this.ensembleJouable.get(numPiece);

                    System.out.println("numpiece  : " + numPiece + " x : " + pieceJoue.getX() + " y : " + pieceJoue.getY());

                }
            }while(!Deplacement.DeplacementPiece(pieceJoue,xDep,  yDep, this.joueurN, 'N'));

            plateau.viderCase();




        }

        sc.close();


    }

    /**
     * Méthode permettant de savoir si le numéro de la pièce selectionnée est valide
     * @param piece, chaine de caractère représentant la piece à valider
     * @return booléen, vrai si la pièce fait partie de l'ensemble jouable sinon faux
     */
    public boolean verifPiece(String piece){
        try {
            int numeroPiece = Integer.parseInt(piece);
            if(numeroPiece>=1 && numeroPiece<=ensembleJouable.size())
            {
                System.out.println("Choix Valide");
                return true;
            }
            System.out.println("Merci d'entrer un autre choix");
            return false;
        } catch (NumberFormatException e){
            System.out.println("Merci d'entrer un autre choix");
            return false;
        }
    } 

    /**
     * Méthode permettant d'afficher le plateau avec les pièces du jeu
     * @param joueur, joueur en cours
     */
    public void AfficherPlateau(Joueur joueur){
        for(int ajoutPiece = 0; ajoutPiece< joueurN.getEnsemblePiece().size(); ajoutPiece++){
            Piece plateauPiece =(Piece) joueurN.getEnsemblePiece().get(ajoutPiece);
            plateau.addPiontPlateau(plateauPiece.getY(), plateauPiece.getX(), plateauPiece.getType());
        }
        System.out.println(plateau.toString());
    }


    /**
     * Méthode permettant d'afficher les pièces jouables
     * @param listPiece, liste de piece jouable
     */
    public void AfficherJouable(ArrayList<Piece> listPiece){
        int numPiece;
        System.out.println("Piece Jouable : ");
        for(int cpt = 0; cpt<listPiece.size();cpt++){
            numPiece = cpt + 1;
            System.out.println(numPiece + " : " + listPiece.get(cpt).getType());
        }
        System.out.println();
    }

    /**
     * Main
     * @param 
     */
    public static void main (String[] args){
        Jeu jeu;

        jeu = new Jeu();        
             
    }
}