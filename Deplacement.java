
import java.util.ArrayList;

/**
 * Classe Deplacement : gère les vérification de déplacement des pièces, le déplacement, la conversion des caractères en entier
 *
 * @author Niels BEAUMONT
 * @version 3.0
 */

public class Deplacement{

/**
 * Méthode permettant de ranger les pièces jouables dans une liste
 * @param plateau, plateau de jeu 
 * @param joueur, joueur en cours
 * @param BorN, caractère indiquant la couleur du joueur
 * @return ArrayList de Piece
 */
    public static ArrayList<Piece> DeplacementDispo(Plateau plateau, Joueur joueur, char BorN){
    	Piece[] pieceTotal = joueur.getAllPiece(); 
    	ArrayList<Piece> pieceValid = new ArrayList();
    	for(int cpt = 0; cpt<pieceTotal.length; cpt++){
    		Piece pieceTmp = pieceTotal[cpt];

    		int x = pieceTmp.getX();
    		int y = pieceTmp.getY();


    		if(pieceTmp.getType().contains("P")&&BorN=='B'){
    			if(joueur.moveValid(x-1, y-1, BorN ) || joueur.moveValid(x, y-1, BorN ) || joueur.moveValid(x+1, y-1, BorN ))
    				{pieceValid.add(pieceTmp);}
    		}
    		if(pieceTmp.getType().contains("P")&&BorN=='N'){
    			if(joueur.moveValid(x-1, y+1, BorN ) || joueur.moveValid(x, y+1, BorN ) || joueur.moveValid(x+1, y+1, BorN ))
    				{pieceValid.add(pieceTmp);}
    		}

    		if(pieceTmp.getType().contains("C")){
    			if(joueur.moveValid(x-1, y+2, BorN ) || joueur.moveValid(x+1, y+2, BorN ) || joueur.moveValid(x-1, y-2, BorN ) || joueur.moveValid(x+1, y-2, BorN ) 
    				|| joueur.moveValid(x-2, y+1, BorN ) || joueur.moveValid(x+2, y+1, BorN ) || joueur.moveValid(x-2, y-1, BorN ) || joueur.moveValid(x-2, y-1, BorN ))
    				{pieceValid.add(pieceTmp);}
    		}

    		if(pieceTmp.getType().contains("T") || pieceTmp.getType().contains("Q") || pieceTmp.getType().contains("R")){
    			if(joueur.moveValid(x-1, y, BorN ) || joueur.moveValid(x+1, y, BorN ) || joueur.moveValid(x, y-1, BorN ) || joueur.moveValid(x, y+1, BorN ))
    				{pieceValid.add(pieceTmp);}
    		}


    		if(pieceTmp.getType().contains("F") || pieceTmp.getType().contains("Q")  || pieceTmp.getType().contains("R")){
    			if(joueur.moveValid(x-1, y-1, BorN ) || joueur.moveValid(x+1, y+1, BorN ) || joueur.moveValid(x+1, y-1, BorN ) || joueur.moveValid(x-1, y+1, BorN ))
    				{pieceValid.add(pieceTmp);}
    		}

    	}

    	return pieceValid;

    }

    //public boolean deplacement(Plateau plateau, Piece piece){}

    /**
     * Méthode permettant le déplacement des pièces
     * @param piece, piece déplacée
     * @param xDep, coordonnée x de déplacement
     * @param yDep, coordonnée y de déplacement
     * @param joueur, joueur en cours
     * @param BorN, caractère représentant la couleur du joueur
     * @return booléen, vrai si la piece est déplacée sinon faux
     */
    public static boolean DeplacementPiece(Piece piece, int xDep, int yDep, Joueur joueur, char BorN ){
        
        if ((piece.getType().substring(0,1).equals("P") && BorN == 'B' )){

        		if( yDep+1 == piece.getY() && (xDep == piece.getX() || xDep+1 == piece.getX() || xDep-1 == piece.getX())){

        			if(joueur.moveValid(xDep, yDep, BorN )){
        				piece.setX(xDep);
        				piece.setY(yDep);

        				return true;

        			}
        		}

        }

        if ((piece.getType().substring(0,1).equals("P") && BorN == 'N' )){

        		if( yDep-1 == piece.getY() && (xDep == piece.getX() || xDep+1 == piece.getX() || xDep-1 == piece.getX())){

        			if(joueur.moveValid(xDep, yDep, BorN )){
        				piece.setX(xDep);
        				piece.setY(yDep);

        				return true;

        			}
        		}

        }

        if (piece.getType().substring(0,1).equals("C")){


        	if((xDep == piece.getX()-1 && yDep == piece.getY()+2) || (xDep == piece.getX()+1 && yDep == piece.getY()+2) || (xDep == piece.getX()-1 && yDep == piece.getY()-2) 
        		|| (xDep == piece.getX()+1 && yDep == piece.getY()-2) || (xDep == piece.getX()-2 && yDep == piece.getY()+1) || (xDep == piece.getX()+2 && yDep == piece.getY()+1)
        		|| (xDep == piece.getX()-2 && yDep == piece.getY()-1) || (xDep == piece.getX()-2 && yDep == piece.getY()-1)){
        		        	System.out.println("puteputepute");
        		if(joueur.moveValid(xDep, yDep, BorN )){
        			piece.setX(xDep);
        			piece.setY(yDep);

        			return true;

        		}
        	}
        }

        if(piece.getType().contains("T") || piece.getType().contains("Q") || piece.getType().contains("R")){

    		if(yDep == piece.getY() || xDep == piece.getX() && !(yDep == piece.getY() && xDep == piece.getX())){
			    if(joueur.moveValid(xDep, yDep, BorN )){
        			piece.setX(xDep);
        			piece.setY(yDep);
       				return true;
       			}
       		}
    	}

    	if(piece.getType().contains("F") || piece.getType().contains("Q") || piece.getType().contains("R")){
    		if(yDep != piece.getY() || xDep != piece.getX() && !(yDep == piece.getY() && xDep == piece.getX())){
			    if(joueur.moveValid(xDep, yDep, BorN )){
        			piece.setX(xDep);
        			piece.setY(yDep);
       				return true;
       			}
			}

    	}





        System.out.println("perdu");
        return false;
    }

/**
 * Méthode permettant de convertir la position x du plateau de chaine de caractère à un entier
 * @param str, chaine de caractère représentant la position en ligne
 * @return int, position x
 */
        public static int charIntoInt(String str){
        switch(str){
            case "A": return 0;
            case "B": return 1;
            case "C": return 2;
            case "D": return 3;
            case "E": return 4;
            case "F": return 5;
            case "G": return 6;
            case "H": return 7;
            default: return -1;
        }
    }
}