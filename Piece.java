/**
 * Classe Piece : Construit les pièces du jeu 
 */

public class Piece 
{
	private String type;
	private int x,y;
	private char couleur;

	/**
	 * Constructeur de la classe Piece
	 * @param couleur, couleur du joueur
	 * @param type, type de la pièce
	 * @param x, position de coordonnée x
	 * @param y, position de coordonnée y
	 * 
	 */
	public Piece(char couleur,String type, int x, int y)
	{
		/*  TB : Tour  blanche
			TN : Tour noire  
			CB : Cavalier Blanc
			CN : Cavalier Noir
			FB : Fou Blanc
			FN : Fou Noir
			Q  : Reine B ou N
			R  : Roi N ou N
			P  : Pion b ou N
		*/
		this.couleur=couleur;
		this.type = type; 
		this.x    = x;
		this.y    = y;
	}
	/*
	Setteurs de la classe Piece
	 */
	public void setX(int x){this.x=x;}
	public void setY(int y){this.y=y;}
	public void setCouleur(char couleur){this.couleur=couleur;}


/*
Getteurs de la classe Piece
 */
	public int getX(){return this.x;}
	public int getY(){return this.y;}

	public char getCouleur(){return this.couleur;}
	public String getType(){return this.type;}


	
}