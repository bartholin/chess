/**
 * Classe Plateau : gère l'ensemble de l'affichage en CUI 
 *
 * @author Niels BEUAMONT
 * @version 3.0
 */

public class Plateau {

    private String plateau[][] = new String[8][8];

    /**
     * Constructeur de la classe Plateau
     * @return
     */
    public Plateau(){
        for(int lig =0; lig < plateau.length; lig++)
            for(int col = 0; col < plateau[0].length; col++)
                this.plateau[lig][col] = "  ";
        
        
    }

    /*
    Getteurs de la classes Plateau
     */
    public int getNbLigne(){return this.plateau.length; }
    public int getNbColonne(){return this.plateau[0].length;}

    /**
     * ToString permettant l'affichage du plateau en CUI
     */
    public String toString(){
        String s="";
        for(int lig = 0; lig < plateau.length; lig++){
            s += "  -------------------------\n";
            for(int col = 0; col< plateau[0].length; col++){
                if (col == 0){
                    s += lig+1 + " |";
                }
                s += plateau[lig][col] + "|";
            }
            s+="\n";
            if(lig == plateau.length-1){
                s +=("  -------------------------\n");
                s +="   A  B  C  D  E  F  G  H\n";
            }
        }
        return s;
    }

    /**
     * Méthode permettant d'ajouter une pièce sur le tableau
     * @param lig, position de la ligne sur le plateau
     * @param col, position de la colonne sur le plateau
     * @param type, type de la pièce
     */
    public void addPiontPlateau(int lig, int col, String type){
        this.plateau[lig][col]= type;
    }

    /**
     * Méthode permettant de vider le tableau
     */
    public void viderCase(){
        for(int x =0; x < plateau.length; x++)
            for(int y = 0; y < plateau[0].length; y++)
                this.plateau[x][y] = "  ";
    } 


    /**
     * Méthode qui renvoie une case du plateau donnée
     * @param lig, position en ligne du plateau
     * @param col, position en colonne du plateau
     * @return String, chaine de caractère représentant une case du plateau
     */
    public String getCase(int lig, int col){
        return this.plateau[lig][col];
    }



}