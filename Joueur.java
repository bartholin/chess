import java.util.ArrayList;

/**
 * Classe Joueur : Construit les deux joueurs, initialise les 16 pièces de chaques joueurs et les enregistres dans des listes
 *
 * @author Niels BEAUMONT
 * @version 3.0
 */

public class Joueur
{
    private Piece[] piece = new Piece[16];
    private static ArrayList<Piece> ensemblePiece = new ArrayList();

    /**
     * Constructeur de la classe Joueur
     * @param couleur
     * 
     */
    public Joueur(char couleur)
    {
        
        if(couleur=='B')
        {
            this.piece[0] = new Piece('B',"T1",0,7);
            this.piece[1] = new Piece('B',"T2",7,7);
            this.piece[2] = new Piece('B',"C1",1,7);
            this.piece[3] = new Piece('B',"C2",6,7);
            this.piece[4] = new Piece('B',"F1",2,7);
            this.piece[5] = new Piece('B',"F2",5,7);
            this.piece[6] = new Piece('B',"Q",3,7);
            this.piece[7] = new Piece('B',"R",4,7);

            for (int cpt = 0; cpt < 8; cpt++ )
                this.piece[cpt+8] = new Piece('B',"P"+cpt,cpt,6);

            for (int cpt = 0; cpt < 16; cpt++ )
                this.ensemblePiece.add(this.piece[cpt]);

        }else{
            this.piece[0] = new Piece('N',"T1",0,0);
            this.piece[1] = new Piece('N',"T2",7,0);
            this.piece[2] = new Piece('N',"C1",1,0);
            this.piece[3] = new Piece('N',"C2",6,0);
            this.piece[4] = new Piece('N',"F1",2,0);
            this.piece[5] = new Piece('N',"F2",5,0);
            this.piece[6] = new Piece('N',"Q",3,0);
            this.piece[7] = new Piece('N',"R",4,0);

            for (int cpt = 0; cpt < 8; cpt++ )
                this.piece[cpt+8] = new Piece('N',"P"+cpt,cpt,1);

            for (int cpt = 0; cpt < 16; cpt++ )
                this.ensemblePiece.add(this.piece[cpt]);
        
        }

        
        
    }

/*
Getteurs de la classe Joueur
 */
    public ArrayList getEnsemblePiece(){
        return this.ensemblePiece;
    }

    public Piece getPieceJoueur(int cpt){
        return this.piece[cpt]; 
    }

    public Piece[] getAllPiece(){
        return this.piece;
    }

    public int getNbPiece(){
        return this.piece.length;
    }

    public Piece getPieceByCoord(int x, int y){
        for(int cpt = 0; cpt < this.ensemblePiece.size(); cpt++){
            if(this.ensemblePiece.get(cpt).getX() == x && this.ensemblePiece.get(cpt).getY() == y){
                return this.ensemblePiece.get(cpt);
            }
        }
        return null;
    }

    /**
     * @param x, position x de la pièce
     * @param y, position y de la pièce
     * @param BorN, couleur du joueur
     * @return booléen, vrai si la position du déplacement est valide
     */
    public boolean moveValid(int x, int y, char BorN){
        if(x>7||x<0||y>7||y<0){return false;}
        for(int cpt=0; cpt<this.ensemblePiece.size(); cpt++){
            if(x==this.ensemblePiece.get(cpt).getX()&&y==this.ensemblePiece.get(cpt).getY())
                if(this.ensemblePiece.get(cpt).getCouleur() == BorN)
                    return false;

        }
        return true;
    }
}